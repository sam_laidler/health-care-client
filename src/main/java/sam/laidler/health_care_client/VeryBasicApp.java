package sam.laidler.health_care_client;

import sam.laidler.ejbclient.RemoteClient;
import sam.laidler.health_care.domain.Patient;
import sam.laidler.health_care.service.PatientManagementException;
import sam.laidler.health_care.service.PatientManagementInterface;

public class VeryBasicApp 
{
    public static void main( String[] args )
    {
        RemoteClient client = new RemoteClient("172.17.0.2", 8080, "sammy", "sammy123!");
        Object obj = client.performLookup("PatientManagement", "sam.laidler.health_care.service.PatientManagementInterface");
        if (obj instanceof PatientManagementInterface) {
        	PatientManagementInterface patientSystem = (PatientManagementInterface) obj;
        	try {
        		patientSystem.registerPatient(new Patient("Dan", "Rogers", 444));
				patientSystem.registerPatient(new Patient("John", "Jones", 555));
	        	patientSystem.registerPatient(new Patient("Tom", "Smith", 666));
	        	patientSystem.registerPatient(new Patient("John", "Smith", 777));
	        	patientSystem.registerPatient(new Patient("John", "Smith", 777));
	        	patientSystem.registerPatient(new Patient("John", "Barrowman", 888));
			} catch (Exception e) {
				/*
				 * TODO: i don't know why but the exception i throw server side is not the one i
				 * catch here. i will have to come back to this some day.
				 */
				System.out.println("Patient already added");
			}
        	System.out.println("Printing all patients...");
        	patientSystem.getPatients().forEach(name -> System.out.println(name));
        	System.out.println("Printing only patients with the family name Smith...");
        	patientSystem.getPatientsByFamilyName("Smith").forEach(name -> System.out.println(name));
        }
    }
}
