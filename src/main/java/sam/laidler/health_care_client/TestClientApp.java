package sam.laidler.health_care_client;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

import sam.laidler.ejbclient.RemoteClient;
import sam.laidler.health_care.domain.Patient;
import sam.laidler.health_care.service.PatientManagementException;
import sam.laidler.health_care.service.PatientManagementInterface;

public class TestClientApp {
	private static void printOptions() {
		System.out.println("\nChoose option");
		System.out.println("1 = register new patient");
		System.out.println("2 = remove patient");
		System.out.println("3 = print all patients");
		System.out.println("4 = print patients by family name");
		System.out.println("5 = exit");
		System.out.println("6 = print these options\n");
	}
	
	private static void registerPatient(Scanner scan, PatientManagementInterface patientManagement) {
    	System.out.println("Enter first name");
    	String firstName = scan.nextLine();

    	System.out.println("Enter family name");
    	String familyName = scan.nextLine();

    	System.out.println("Enter NHS number");
    	String nhsNo= scan.nextLine();

		/*
		 * TODO: i don't know why but the exception i throw server side is not the one i
		 * catch here. i will have to come back to this some day.
		 */
    	try {
			patientManagement.registerPatient(new Patient(firstName, familyName, Integer.parseInt(nhsNo)));
		} catch (Exception e) {
			System.out.println("Patient already registered");
		}
	}
	
	private static void removePatient(Scanner scan, PatientManagementInterface patientManagement) {
    	System.out.println("Enter NHS number of patient to be removed");
    	int nhsNo = scan.nextInt();
    	Patient patient = null;
		/*
		 * TODO: i don't know why but the exception i throw server side is not the one i
		 * catch here. i will have to come back to this some day.
		 */
		try {
			patient = patientManagement.getPatientByNHSNumber(nhsNo);
		} catch (Exception e) {
			System.out.println("Patient not registered");
			return;
		}
    	patientManagement.removePatient(patient);
	}
	
	private static void printPatients(PatientManagementInterface patientManagement) {
    	System.out.println("Printing all patients...");
    	patientManagement.getPatients().forEach(name -> System.out.println(name));
	}
	
	private static void printPatientsByFamilyName(Scanner scan, PatientManagementInterface patientManagement) {
    	System.out.println("Enter family name of patients to be printed");
    	String familyName = scan.nextLine();

    	System.out.println("Printing patients...");
    	patientManagement.getPatientsByFamilyName(familyName).forEach(name -> System.out.println(name));
	}
	
	private static int getOption(Scanner scan) {
		try {
			return scan.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Bad option selected");
			return 6;
		}
	}
	
	public static void main(String[] args) throws IOException {
		RemoteClient client = new RemoteClient("172.17.0.2", 8080, "sammy", "sammy123!");
		Object obj = client.performLookup("PatientManagement",
				"sam.laidler.health_care.service.PatientManagementInterface");
		if (obj instanceof PatientManagementInterface) {
			PatientManagementInterface patientManagement = (PatientManagementInterface) obj;
			
			printOptions();
			
			Scanner scan = new Scanner(System.in);
			int option = 6;
			while((option = getOption(scan)) != 5) {
				// consume any newline
				scan.nextLine();
                switch(option) {
                case 1:
                	registerPatient(scan, patientManagement);
                	break;
                case 2:
                	removePatient(scan, patientManagement);
                	break;
                case 3:
                	printPatients(patientManagement);
                	break;
                case 4:
                	printPatientsByFamilyName(scan, patientManagement);
                	break;
                case 6:
                	// just fall thru as options printed each loop
                	break;
                default:
                	System.out.println("Unsupported option");
                }
				printOptions();
			}
			System.out.println("Exiting...");
		}
	}
}
